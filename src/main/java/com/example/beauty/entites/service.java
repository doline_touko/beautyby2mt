package com.example.beauty.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class service {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idService;

    private String nomService;

    private String image;

    private String description;

    private String prix;

    @ManyToOne
    @JoinTable( name = "Admin_services",
            joinColumns = @JoinColumn(name = "idService"),
            inverseJoinColumns = @JoinColumn(name = "idUser"))
    private utilisateur utilisateur;
}
