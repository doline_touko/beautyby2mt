package com.example.beauty.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class produit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idProduit;

    private String nom;

    private String image;

    private String description;

    private String prix;

    @ManyToOne
    @JoinTable( name = "Produits_admin",
            joinColumns = @JoinColumn(name = "idProduit"),
            inverseJoinColumns = @JoinColumn(name = "idUser"))
    private utilisateur utilisateur;

    @ManyToOne
    @JoinTable( name = "Produits_prestataire",
            joinColumns = @JoinColumn(name = "idProduit"),
            inverseJoinColumns = @JoinColumn(name = "idPrestataire"))
    private prestataire prestataire;

}
