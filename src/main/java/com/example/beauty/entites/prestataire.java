package com.example.beauty.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class prestataire {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idPrestataire;

    private String nom;

    private String prenom;

    private String domaine;

    private String region;

    @OneToMany
   @JoinTable( name = "Produits_prestataire",
            joinColumns = @JoinColumn(name = "idPrestataire"),
           inverseJoinColumns = @JoinColumn(name = "idProduit"))
    private List<produit> produits= new ArrayList<>();


}
