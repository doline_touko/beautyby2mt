package com.example.beauty.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class utilisateur {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idUser;

    private String nom;

    private String prenom;

    private String email;

    private String tel;

    @OneToMany
    @JoinTable( name = "Produits_admin",
            joinColumns = @JoinColumn(name = "idUser"),
            inverseJoinColumns = @JoinColumn(name = "idProduit"))
    private List<produit> produits= new ArrayList<>();
    @OneToMany
    @JoinTable( name = "Admin_services",
            joinColumns = @JoinColumn(name = "idUser"),
            inverseJoinColumns = @JoinColumn(name = "idService"))
    private List<produit> services= new ArrayList<>();


}
