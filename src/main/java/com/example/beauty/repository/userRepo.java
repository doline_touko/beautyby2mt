package com.example.beauty.repository;

import com.example.beauty.entites.utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface userRepo extends JpaRepository<utilisateur,Long> {
}
