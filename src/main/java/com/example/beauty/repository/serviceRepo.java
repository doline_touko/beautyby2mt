package com.example.beauty.repository;

import com.example.beauty.entites.service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface serviceRepo extends JpaRepository<service,Long> {
}
