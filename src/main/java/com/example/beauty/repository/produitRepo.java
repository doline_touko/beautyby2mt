package com.example.beauty.repository;

import com.example.beauty.entites.produit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface produitRepo extends JpaRepository<produit,Long> {
}
