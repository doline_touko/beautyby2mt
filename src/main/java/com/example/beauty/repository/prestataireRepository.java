package com.example.beauty.repository;

import com.example.beauty.entites.prestataire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface prestataireRepository extends JpaRepository<prestataire, Long> {

}
