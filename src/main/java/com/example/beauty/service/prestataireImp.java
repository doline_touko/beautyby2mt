package com.example.beauty.service;

import com.example.beauty.entites.prestataire;
import com.example.beauty.repository.prestataireRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class prestataireImp implements prestataireService {

    @Autowired
    private prestataireRepository repository;

    @Override
    public List<prestataire> listePrestataire() {
        return repository.findAll();
    }

    @Override
    public prestataire rechercherPrestataire(Long id) {
        return repository.getById(id);
    }

    @Override
    public prestataire enregistrerPrestataire(prestataire prestataire) {
        return repository.save(prestataire);
    }

    @Override
    public void supprimerPrestataire(Long id) {

    }
}
