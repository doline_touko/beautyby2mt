package com.example.beauty.service;

import com.example.beauty.entites.produit;

import java.util.List;

public interface produitService {
    List<produit> listeProduit();
    produit rechercherProduit(Long id);
    produit enregistrerProduit(produit produit);
    void supprimerProduit(Long id);
}
