package com.example.beauty.service;

import com.example.beauty.entites.prestataire;

import java.util.List;

public interface prestataireService {

    List<prestataire> listePrestataire();
    prestataire rechercherPrestataire(Long id);
    prestataire enregistrerPrestataire(prestataire prestataire);
    void supprimerPrestataire(Long id);
}
