package com.example.beauty.service;

import com.example.beauty.entites.utilisateur;

import java.util.List;

public interface utilisateurService {
    List<utilisateur> listeUtilisateur();
    utilisateur enregistrerUtilisateur(utilisateur user);
}
