package com.example.beauty.service;

import com.example.beauty.entites.service;

import java.util.List;

public interface Iservice {
    List<service> listeService();
    service rechercherService(Long id);
    service enregistrerService(service service);
    void supprimerService(Long id);
    //List<service> filtrerParNomOuRegion(String mot_cle);
}
