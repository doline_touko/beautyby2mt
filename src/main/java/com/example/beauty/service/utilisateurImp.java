package com.example.beauty.service;

import com.example.beauty.entites.utilisateur;
import com.example.beauty.repository.userRepo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class utilisateurImp implements utilisateurService {

    @Autowired
    private userRepo repository;

    @Override
    public List<utilisateur> listeUtilisateur() {
        return repository.findAll();
    }

    @Override
    public utilisateur enregistrerUtilisateur(utilisateur user) {
        return repository.save(user);
    }
}
