package com.example.beauty.service;

import com.example.beauty.entites.service;
import com.example.beauty.repository.serviceRepo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class IserviceImp implements Iservice{

    @Autowired
    private serviceRepo repository;

    @Override
    public List<service> listeService() {
        return repository.findAll();
    }

    @Override
    public service rechercherService(Long id) {
        return repository.getById(id);
    }

    @Override
    public service enregistrerService(service service) {
        return repository.save(service);
    }

    @Override
    public void supprimerService(Long id) {

    }
}
