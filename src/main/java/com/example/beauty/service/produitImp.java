package com.example.beauty.service;

import com.example.beauty.entites.produit;
import com.example.beauty.repository.produitRepo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class produitImp implements produitService {

    @Autowired
    private produitRepo repository;

    @Override
    public List<produit> listeProduit() {
        return repository.findAll();
    }

    @Override
    public produit rechercherProduit(Long id) {
        return repository.getById(id);
    }

    @Override
    public produit enregistrerProduit(produit produit) {
        return repository.save(produit);
    }

    @Override
    public void supprimerProduit(Long id) {

    }
}
